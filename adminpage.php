<?php
    session_start();
    include("db.php");
    include("navbar.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
</head>
<body>
<?php 
    echo"<br><h3 id='title'>Termini</h3>";
    echo"<div id='vecicont'>";
    $member_id = $_SESSION['id'];
    $sql = "SELECT t.idT, t.usluga, t.datum, t.sati, t.djelatnik, t.memberid, t.prihvaceno, t.porukaadmina, m.id, m.firstname, m.lastname FROM terms AS t INNER JOIN members AS m ON(t.memberid=m.id) ORDER BY t.idT DESC";
    $result=$con->query($sql);
    if($result->num_rows > 0){
        while($row = $result->fetch_assoc()){
        $idt=$row["idT"];
        $usluga=$row["usluga"];
        $datum=$row["datum"];
        $sati=$row["sati"];
        $djelatnik=$row["djelatnik"];
        $memberid= $row["memberid"];
        $ime = $row['firstname'];
        $prezime = $row['lastname'];
        $prihvaceno = $row['prihvaceno'];
        $porukaadmina = $row['porukaadmina'];
        echo"<div id='cont'>
        <form id='forma' action='updatetermadmin.php' method='post'>
        <input type='hidden' name='id' value='$idt'>
        <strong>Usluga:</strong> $usluga<br>
        <strong>Datum:</strong> $datum<br>
        <strong>Sati:</strong> $sati<br>
        <strong>Djelatnik:</strong> $djelatnik<br>
        <strong>Klijent:</strong> $ime $prezime<br><br>";
       
        if($prihvaceno==0){
            echo" <input type='text' name='poruka' placeholder='Unesite alternativni prijedlog termina'><br>
        <strong>Prihvatite termin: </strong><button class='btn btn-outline-primary m-2' name='submit' type='submit'>Potvrdi</button><br>";
        }else{
            echo "<strong>Prihvatili ste termin</strong><br>";
            if($porukaadmina!=null){
                echo"Pod uvjetom: $porukaadmina<br>";
            }
        }
            echo"Ako želite, možete obrisati termin:<button class='btn btn-outline-primary m-2' name='delete' type='submit'>Delete</button>
        </form>
        </div>";
        
        }
    }
    ?>        
    </div>
</body>
</html>
<style>
    body{
        width:100%;
        margin: auto;
        justify-content: center;
        background-image: url("src/adminbckgr.jpg");
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
    }
    #vecicont{
        display: flex;
        margin: auto;
        flex-wrap: wrap;
        justify-content: center;
    }
    #cont{        
        background-color: white;
        width: 25%;
        margin: 10px;
        box-shadow: 2px 2px 5px grey;
        font-family: Arial, Helvetica, sans-serif;
        padding: 8px;
        flex-wrap: wrap;
 
    }

    #title{
        text-align: center;
        margin: 5px 20px;
        font-family: 'Dancing Script', cursive;
        font-size: 60px;
        color: white;
        text-shadow: 2px 2px 5px grey;
    }
    #forma{
        margin: 2px;
    }
    input[type=text] {
        width: 90%;
        padding: 15px;
        margin: 10px;
        border: none;
        background: #f1f1f1;
    }
    input[type=text]:focus {
        background-color: #ddd;
        outline: none;
    }
</style>