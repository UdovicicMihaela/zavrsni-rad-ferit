<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>
<body>
<div id="form-wrapper">
    <form class="container" name="myForm" method="post" action="signin.php">
        <h1>Prijava</h1>
        <input type="email" placeholder="Enter Email" name="email" required>
        <input type="password" placeholder="Enter Password" name="psw">
        <button class="btn" name='submit' type="submit">Prijavi se</button>
        <div id="fora"><button id="reset" name='reset' type="submit">Zaboravili ste lozinku?</button></div>
    </form>
</div>
</body>
</html>

<style>
body, html{
    height: 100%;
    font-family: Arial, Helvetica, sans-serif;
    margin: auto;
}
*{
    box-sizing: border-box;
}
#form-wrapper{
    background-image: url("src/reg-unsplash.jpg");
    
    height: 100%;
    width: 100%;
    text-align: center;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    position: relative;
}

.container{
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    margin: 20px;
    max-width: 300px;
    padding: 16px;
    background-color:white;
    box-shadow: 2px 2px 5px grey;
}

.container h1{
    text-align: center;
}

input[type=password], input[type=email] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}
input[type=password]:focus, input[type=email]:focus {
  background-color: #ddd;
  outline: none;
}

.btn {
  background-color: #04AA6D;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.btn:hover {
  opacity: 1;
}
#reset{
  text-decoration: none;
  border: none;
  background-color: white;
  color: blue;
  cursor: pointer;
}
#fora{
  margin-top: 8px !important;
}
</style>