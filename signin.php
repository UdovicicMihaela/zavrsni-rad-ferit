<?php
    use PHPMailer\PHPMailer\PHPMailer;
    session_start();
    include("db.php");

    $mail=filter_input(INPUT_POST,'email');
    $pass=filter_input(INPUT_POST,'psw');

    if(isset($_POST['submit'])){
    if(!empty($mail)){
        if(!empty($pass)){
            $stmt = $con->prepare("SELECT email, pass FROM members WHERE email='$mail'"); 
            $stmt->bind_param("s", $mail); 
            $stmt->execute();
            $stmt->store_result();

            $stmt->bind_result($uname, $pw);
            if($stmt->num_rows == 1){
                $stmt->fetch();
                if(password_verify($pass, $pw)){
                    $user_query = "SELECT * FROM members WHERE email='$mail'";
                    $run = mysqli_query($con, $user_query);
                    while($row = mysqli_fetch_array($run)){
                        $ime = $row['firstname'];
                        $prezime = $row['lastname'];
                        $user_id = $row['id']; 
                        $is_admin = $row['is_admin'];    
                    }
                    
                    $_SESSION['ime'] = $ime;
                    $_SESSION['prezime'] = $prezime;
                    $_SESSION['id'] = $user_id;
                    $_SESSION["loggedin"] = true;
                    $_SESSION["is_admin"] = $is_admin;
                
                    echo "<script>window.open('home.php', '_self')</script>";
                    exit();
                }else{
                    echo "<script>alert('Pogrešno unesen email ili lozinka!')</script>";
                    echo "<script>document.location='login.php'</script>";
                }
                $con->close();
            }else{
                echo "<script>alert('Unesi lozinku!')</script>";
                
                die();
                echo "<script>document.location='login.php'</script>";
            }
        }else{
            echo "<script>alert('Unesi email!')</script>";
            die();
            echo "<script>document.location='login.php'</script>";

        }            
    }
  }

  if(isset($_POST['reset'])){
    if(!empty($mail)){
      $uquery = "SELECT * FROM members WHERE email='$mail'";
      $result = mysqli_query($con, $uquery);
      $row = mysqli_num_rows($result);
      if($row == ""){
        echo "<script>alert('Email ne postoji!')</script>";
        echo "<script>document.location='login.php'</script>";
      }
      $output = '';
      $expFormat = mktime(date("H"), date("i"), date("s"), date("m"), date("d") + 1, date("Y"));
      $expDate = date("Y-m-d H:i:s", $expFormat);
      $key = md5(time());
      $addKey = substr(md5(uniqid(rand(), 1)), 3, 10);
      $key = $key . $addKey;
      // Insert Temp Table
      $que = "INSERT INTO password_reset_temp(email, keyy, expDate) VALUES ('".$mail."','".$key."','".$expDate."')";

      mysqli_query($con, $que);
      $output.='<p>Please click on the following link to reset your password.</p>';
      //replace the site url
      $output.='<p><a href="http://localhost/zavrsni/reset-password.php?key= '.$key.' &email= '.$mail.' &action=reset" target="_blank">http://localhost/zavrsni/reset-password.php?key='.$key.'&email='.$mail.'&action=reset</a></p>';
      $body = $output;
      $subject = "Password Recovery";
      $email_to = $mail;
      //autoload the PHPMailer
      require("vendor/autoload.php");
      $mail = new PHPMailer();
      $mail->IsSMTP();
      $mail->Host = "smtp.gmail.com"; 
      $mail->Username = "mihaelaudovicic4@gmail.com"; // Enter your email here
      $mail->Password = ""; //Enter your password here
      $mail->SMTPSecure = "tls";
      $mail->Port = 587;
      $mail->SMTPAuth = true;
      $mail->SMTPDebug = 4;
      $mail->AuthType='LOGIN';
      $mail->IsHTML(true);
      $mail->From = "mihaelaudovicic4@gmail.com";
      $mail->FromName = "Mihaela";

      $mail->Subject = $subject;
      $mail->Body = $body;
      $mail->AddAddress($email_to);
      if (!$mail->Send()) {
          echo "<script>alert('Mailer Error: " . $mail->ErrorInfo . "')</script>";
      } else {
          echo "<script>alert('An email has been sent')</script>";
          echo "<script>document.location='login.php'</script>";
      }
                        
    }else{
      echo "<script>alert('Unesi email!')</script>";
      die();
    } 
  }

?>