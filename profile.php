<?php
session_start();
include("db.php");
include("navbar.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <?php 
        echo"<br><h3 id='title'>Moji termini</h3>";
        $member_id = $_SESSION['id'];
        $sql = "SELECT * FROM terms WHERE memberid='$member_id'";

        $result=$con->query($sql);
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
            $idt=$row["idT"];
            $usluga=$row["usluga"];
            $datum=$row["datum"];
            $sati=$row["sati"];
            $djelatnik=$row["djelatnik"];
            $prihvaceno=$row["prihvaceno"];
            $porukaadmina=$row['porukaadmina'];
            
            
            echo"<div id='cont'>
            <form id='forma' action='updateterm.php' method='post'>
            <input type='hidden' name='id' value='$idt'>
             Usluga:<input type='text' name='usluga' value='$usluga'><br>
             Datum:<input type='date' name='datum' value='$datum'><br>
             Sati:<input type='text' name='sati' value='$sati'><br>
             Djelatnik:<select name='djelatnik' id='djelatnik'>
                <option value='$djelatnik' selected>$djelatnik</option>
                <option>Emilija</option>
                <option>Jasna</option>
                <option>Iva</option>
            </select><br><br>
            
            Termin prihvaćen: "; 
            if($prihvaceno==1)
            {
                echo"<i class='fas fa-check' id='span1' style='color:green;text-shadow: 2px 2px 2px grey;'></i><br>";
                if($porukaadmina!=""){ echo"<strong>Pod uvjetom:</strong> $porukaadmina<br>"; }
            }
            else
            { 
                echo"<i class='fas fa-times' id='span2' style='color:red;text-shadow: 2px 2px 2px grey;'></i><br> ";
            }
            echo"
            <br>
            <button class='btn btn-outline-primary m-2' name='submit' type='submit'>Update</button>
            <button class='btn btn-outline-primary' name='delete' type='submit'>Delete</button>
            <br>
            </form>
            </div>";
            }
        }
    ?>    
    
</body>
</html>


<style>
    body{
        width:100%;
        
        margin: auto;
        justify-content: center;
        background-image: url("src/adminbckgr.jpg");
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
    }
    #cont{
        width: 50%;
        margin: 10px;
        box-shadow: 2px 2px 5px grey;
        font-family: Arial, Helvetica, sans-serif;
        padding: 8px;
        background-color:white;
        opacity: 0.95;
    }
    input[type=text], input[type=date] {
        width: 60%;
        padding: 15px;
        margin: 10px;
        border: none;
        background: #f1f1f1;
    }
    input[type=text]:focus, input[type=date]:focus {
        background-color: #ddd;
        outline: none;
    }
    #djelatnik {
        width: 60%;
        padding: 15px;
        margin: 10px;
        border: none;
        background: #f1f1f1;
    }
    #djelatnik:focus  {
        background-color: #ddd;
        outline: none;
    }
    #title{
        text-align: center;
        margin: 5px 20px;
        font-family: 'Dancing Script', cursive;
        font-size: 60px;
        color: white;
        text-shadow: 2px 2px 5px grey;
        
    }
    #forma{
        margin: 2px;
    }
</style>