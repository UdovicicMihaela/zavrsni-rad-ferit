<?php
    include("db.php");
    session_start();
    $target_dir = "galerija/";
    $uploadOk = 1;

    if(isset($_POST["submit"])) {
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $image=$_FILES['fileToUpload']['name'];
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    
    if($check !== false) {
      $uploadOk = 1;
    } else {
      echo "<script>alert('File is not an image.')</script>";
      $uploadOk = 0;
    }
  
    if (file_exists($target_file)) {
      echo "<script>alert('Sorry, file already exists.')</script>";
      $uploadOk = 0;
    }
    if ($_FILES["fileToUpload"]["size"] > 1500000) {
      echo "<script>alert('Sorry, your file is too large.')</script>";
      $uploadOk = 0;
    }
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
      echo "<script>alert('Sorry, only JPG, JPEG, PNG & GIF files are allowed.')</script>";
      $uploadOk = 0;
    }

    if ($uploadOk == 0) {
      echo "<script>alert('Sorry, your file was not uploaded.')</script>";
    }else{
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                    
            $query="INSERT INTO galery(img) VALUES ('$image')";
            
            $run_insert=mysqli_query($con,$query);
            if($run_insert){
                echo "<script>alert('Uspješno ste unijeli u bazu!')</script>";
            }else{
                echo "<script>alert('Error occured')</script>";
            }
            mysqli_close($con);
           echo "<script>window.open('galerija.php', '_self')</script>";
        }
    }
    echo "<script>window.open('galerija.php', '_self')</script>";
}

?>