<?php
    session_start();
    include("db.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Frizerski salon</title>

    <!--fonts-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@500&display=swap" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <!-- Bootstrap 4 Icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!--carousel-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
    <header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <a class="navbar-brand" href="#">
            <img src="../zavrsni/src/logo.png" width="30" height="30" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="home.php" >Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="usluge.php">Usluge</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="novosti.php" >Novosti</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="galerija.php" >Galerija</a>
            </li>
            <!--<li class="nav-item">
                <a class="nav-link" href="#" >Kontakt</a>
            </li>-->
            </ul>
        </div>
        <form class="d-flex mb-0">
        <?php if(isset($_SESSION["loggedin"])===false){ ?>
            <button class="btn btn-outline-primary mr-2"><a href="login.php" id="a_login">Login</a></button>
            <button class="btn btn-outline-primary"><a href="register.php" id="a_reg">Register</a></button>
        <?php } ?>
        <?php if(isset($_SESSION["loggedin"])===true){ 
            $x=1;
            if($_SESSION["is_admin"]!=$x){ ?>
                <button class="btn btn-outline-primary mr-2"><a href="profile.php" id="a_prof">Moji termini</a></button>
            <?php }else{ ?>
                <button class="btn btn-outline-primary mr-2"><a href="adminpageuserlist.php" id="ul_admin">Korisnici</a></button>
                <button class="btn btn-outline-primary mr-2"><a href="adminpage.php" id="a_admin">Termini</a></button>
            <?php } ?>
            <button class="btn btn-outline-primary"><a href="logout.php" id="a_logout">Logout</a></button>
        <?php } ?>
        </form>
    </nav>
    </header>    

    

</body>
</html>
<style>
    #a_login, #a_reg, #a_logout, #a_prof, #a_admin, #ul_admin{
        text-decoration: none !important;
    }
    #a_login:hover, #a_reg:hover, #a_logout:hover, #a_prof:hover, #a_admin:hover, #ul_admin:hover{
        color: white !important;
    }
    

    
</style>