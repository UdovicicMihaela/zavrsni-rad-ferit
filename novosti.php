<?php
include("db.php");
include("navbar.php");
session_start();
?>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
</head>
<body>
    <br><br>
    <?php if(isset($_SESSION["loggedin"])===true){ 
        $x=1;
        if($_SESSION["is_admin"]==$x){ ?>
        <div id="admin_container">
        <form name="myForm" method="post" action="addarticle.php" enctype="multipart/form-data">
            <input type="text" placeholder="Enter Name Of New Article" name="ime" required>
            <textarea id="new_article" name="novost" rows="3" cols="30" placeholder="Enter New Article" required></textarea>
                    
            Unesi sliku:<br>
            <input type="file" name="fileToUpload" id="fileToUpload" required="required">
            <br>
            <button class="btn btn-success m-2" type="submit" name="submit">Submit</button>
                
        </form>
        </div>
        <br>
    <?php } } ?>
    
    
    <?php
        $quer="SELECT id, title, about, img FROM articles ORDER BY id DESC";
        $response= mysqli_query($con,$quer);
            
        if($response){
            while($row=mysqli_fetch_array($response)){
                $idd = $row['id'];
                $title = $row['title'];
                $buttonid=$row['img'];
                $abt = substr($row['about'], 0, 500);
                $aboutt = $row['about'];

                echo "<div id='container'>
                <p id='about'>
                    <img id='uploaded_img' src='uploads/".$row['img']."'>
                    <strong>".$row['title']."</strong><br>
                    <span id='manje$idd'>".$abt."...</span><span id='vece$idd' style='display:none;'>".$aboutt."</span><br>
                    <button id='btn$idd' onclick='myFunction$idd()' class='btn btn-outline-primary' style='float:right;' >Pročitajte cijeli članak</button>
                </p>
                </div>";

                echo"
                <script>
                function myFunction$idd(){
                    var dots = document.getElementById('manje$idd');
                    var moreText = document.getElementById('vece$idd');
                    var btnText = document.getElementById('btn$idd');
                    if (dots.style.display === 'none') {
                      dots.style.display = 'inline';
                      btnText.innerHTML = 'Pročitajte cijeli članak'; 
                      moreText.style.display = 'none';
                    } else {
                      dots.style.display = 'none';
                      btnText.innerHTML = 'Smanjiti'; 
                      moreText.style.display = 'inline';
                    }
                }

                </script>
                <br>";
                

            }
        }
    ?>
    
</body>
</html>


<style>
#container{
    display:flex;
    width: 70%;
    margin: auto;
    box-shadow: 2px 2px 5px grey;
    padding: 5px;
    flex-wrap: wrap;
    font-family: Arial, Helvetica, sans-serif;
}


#about{
    padding: 5px;
    margin: auto;
    text-align: justify;
}
#uploaded_img{
    height: 30%;
    width: 25%;
    padding: 5px 15px 5px 5px;
    margin: auto;
    float: left;
}
#admin_container{
    width: 70%;
    margin: auto;
    box-shadow: 2px 2px 5px grey;
    padding: 5px;
   
    display: flex;
    align-items: center;
    
}

#new_article{
    width: 100%;
    margin: 5px;
    padding: 15px;
    margin: 5px 0 10px 0;
    border: none;
    background: #f1f1f1;
}
#new_article:focus{
    background-color: #ddd;
    outline: none;
}

input[type=text]{
  width: 100%;
  padding: 15px;
  margin: 15px 0 10px 0;
  border: none;
  background: #f1f1f1;
}
input[type=text]:focus {
  background-color: #ddd;
  outline: none;
}


</style>