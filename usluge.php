<?php
include("db.php");
include("navbar.php");
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <br>
    <div id="glavno">
    <div style="text-align:center">
        <h2>CJENIK Frizerski salon</h2>
    </div>
    <br>
    
    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">Ženske frizure</th>
            <th scope="col">Kratka kosa</th>
            <th scope="col">Normalna kosa</th>
            <th scope="col">Duga kosa</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row">Pranje kose</th>
            <td>40 kn</td>
            <td>50 kn</td>
            <td>60 kn</td>
            </tr>
            <tr>
            <th scope="row">Šišanje</th>
            <td>50 kn</td>
            <td>70 kn</td>
            <td>90 kn</td>
            </tr>
            <tr>
            <th scope="row">Šišanje šiški</th>
            <td>20 kn</td>
            <td>20 kn</td>
            <td>20 kn</td>
            </tr>
            <tr>
            <th scope="row">Fen frizura</th>
            <td>50 kn</td>
            <td>70 kn</td>
            <td>90 kn</td>
            </tr>
            <th scope="row">Vodena frizura</th>
            <td>60 kn</td>
            <td>80 kn</td>
            <td>120 kn</td>
            </tr>
            <th scope="row">Svečane frizure</th>
            <td>150 kn</td>
            <td>220 kn</td>
            <td>280 kn</td>
            </tr>
            <th scope="row">Bojanje</th>
            <td>80 kn</td>
            <td>100 kn</td>
            <td>120 kn</td>
            </tr>
            <th scope="row">Pramenovi površinski jednobojni</th>
            <td>150 kn</td>
            <td>190 kn</td>
            <td>220 kn</td>
            </tr>
            <th scope="row">Pramenovi cijela glava jednobojni</th>
            <td>200 kn</td>
            <td>250 kn</td>
            <td>280 kn</td>
            </tr>
            <th scope="row">Pramenovi površinski dvobojni</th>
            <td>180 kn</td>
            <td>200 kn</td>
            <td>250 kn</td>
            </tr>
            <th scope="row">Pramenovi cijela glava dvobojni</th>
            <td>220 kn</td>
            <td>280 kn</td>
            <td>350 kn</td>
            </tr>
            <th scope="row">Dječje šišanje</th>
            <td>40 kn</td>
            <td>50 kn</td>
            <td>60 kn</td>
            </tr>
            <th scope="row"></th>
            <td></td>
            <td></td>
            <td></td>
            </tr>

        </tbody>
        </table>
       
        <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">Muške frizure</th>
            <th scope="col"></th>
            <th scope="col">Kratka kosa</th>
            <th scope="col">Duga kosa</th>
            
            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row">Pranje kose</th>
            <td></td>
            <td>40 kn</td>
            <td>50 kn</td>
            </tr>
            <th scope="row">Šišanje</th>
            <td></td>
            <td>50 kn</td>
            <td>70 kn</td>
            </tr>
            <th scope="row">Bojanje</th>
            <td></td>
            <td>80 kn</td>
            <td>100 kn</td>
            </tr>
            <tr>
            <th scope="row">Pramenovi</th>
            <td></td>
            <td>150 kn</td>
            <td>190 kn</td>
            </tr>
            <th scope="row">Šišanje s mašinicom jednolično</th>
            <td></td>
            <td>50 kn</td>
            <td>70 kn</td>
            </tr>
            <th scope="row">Šišanje s mašinicom više dužina</th>
            <td></td>
            <td>60 kn</td>
            <td>80 kn</td>
            </tr>
            <th scope="row">Brijanje</th>
            <td></td>
            <td>50 kn</td>
            <td></td>
            </tr>
            <th scope="row">Štucanje brade</th>
            <td></td>
            <td>40 kn</td>
            <td></td>
            </tr>
            <th scope="row">Brijanje glave</th>
            <td></td>
            <td>60 kn</td>
            <td></td>
            </tr>
            <th scope="row">Skidanje brade</th>
            <td></td>
            <td>20 kn</td>
            <td></td>
            </tr>
            <th scope="row">Dječje šišanje</th>
            <td></td>
            <td>40 kn</td>
            <td>50 kn</td>
            </tr>
            <th scope="row"></th>
            <td></td>
            <td></td>
            <td></td>
            </tr>
        </tbody>
    </table>

   
    <?php if(isset($_SESSION["loggedin"])===true){ 
    $x=1;
    if($_SESSION["is_admin"]!=$x){ ?>
    <div class="form-wrapper">
        <form class="container" name="myForm" method="post" action="unesitermin.php">
            <h1>Odaberi termin za naše usluge</h1>
            <input type="text" placeholder="Unesi tip usluge" name="usluga" required>
            <input type="date" placeholder="Unesi datum za termin usluge" name="datum" required>
            <input type="text" placeholder="Unesi u koliko sati" name="sati" required>
            <div class="form-group">
                <label for="FormControlSelect">Odaberite djelatnika</label>
                <select class="form-control" id="FormControlSelect" name="djelatnik">
                    <option>Emilija</option>
                    <option>Jasna</option>
                    <option>Iva</option>
                </select>
            </div>
            <button class="btnsubmit" name='submit' type="submit">Submit</button>
            <br>
        </form><br>     
    </div>
    <?php } } ?>
 
    </div>

</body>
</html>

<style>
    body, html{
        height: 100%;
        
        margin: auto;
    }
    *{
        box-sizing: border-box;
    }
    #glavno{
        font-family: Arial, Helvetica, sans-serif;
    }

    .container{
        position: absolute;
        left: 38%;
        margin: 20px;
        max-width: 400px;
        padding: 16px;
        background-color:white;
        box-shadow: 2px 2px 5px grey;
    }
    @media only screen and (max-width: 750px){
        .container{
            left: 20%;
        }
    }
    .container h1{
        text-align: center;
    }
    #FormControlSelect {
        width: 100%;
        
        margin: 5px 0 22px 0;
        border: none;
        background: #f1f1f1;
    }
    #FormControlSelect:focus  {
        background-color: #ddd;
        outline: none;
    }
    /* Full-width input fields */
    input[type=text], input[type=date] {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        border: none;
        background: #f1f1f1;
    }
    input[type=text]:focus, input[type=date]:focus {
        background-color: #ddd;
        outline: none;
    }
    .btnsubmit {
        background-color: #04AA6D;
        color: white;
        padding: 16px 20px;
        border: none;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
    }

    .btnsubmit:hover {
        opacity: 1;
    }

</style>