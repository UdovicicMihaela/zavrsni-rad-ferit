<?php
include("db.php");
include("navbar.php");
//session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Frizerski salon M</title>
</head>
<body>
<!--        ....CAROUSEL....     -->
<div id="demo" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  
  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item">
      <img src="src/bckgr2.jpg" alt="First">
    </div>
    <div class="carousel-item">
      <img src="src/bckgr3.jpg" alt="Second" >
    </div>
    <div class="carousel-item active">
      <img src="src/bckgr4.jpg" alt="Third">
    </div>
    
  </div>
  
  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
<br>


<!--    ....ABOUT....   -->
<div id="abouthairsaloon">
  <div id="smallerdiv">
  <form class="l-flex">
    <h1 id="glavninaslov">Frizerski salon M</h1>
    <p>Frizerski salon M je otvoren 2012. godine u Osijeku. Kroz godine je postao omiljen salon mnogih zadovoljnih klijenata u kojem uvijek vlada vesela i prijateljska atmosfera!
    Svakom klijentu posvećujemo maksimalnu pažnju te frizure moraju biti savršene na obostrano zadovoljstvo! Višegodišnje iskustvo ,te svakogodišnje pohađanje raznih seminara i edukacija uz veliku upornost dovelo je do toga da nam je svakodnevni posao zadovoljstvo i novi izazov!</p>
    </form>
    <form class="d-flex">
      <img src="src/logo.png" width="130" height="130" alt="AdminPic">
    </form>
  </div>
</div>
<br><br><br><br>


<!-- ....DJELATNICI.... -->
<div class="m-auto" style="display:table;">
  <h1 id="glavninaslov">Naše djelatnice</h1>
</div><br><br>
<div id="bigdjelcont">
  <div id="djelatnikcontainer">
    <div id="firstdjel">
      <img src="src/djel1.jpg" width="330" height="300" style="border-radius:90%;box-shadow: 2px 2px 5px grey;" alt="Djelatnik1">
      <p><strong id="ime">Emilija </strong></p>
     <?php 
     $sqldjel="SELECT ROUND(AVG(ocjenadjelatnika), 1) AS avg FROM comments WHERE djelatnik='Emilija'";
     $result=mysqli_query($con,$sqldjel);
     while($row = mysqli_fetch_array($result)) { 
       echo"<div><br>".$row['avg']." <i class='fas fa-star' style='color:gold;text-shadow: 2px 2px 4px grey;'></i></div>"; 
      }
     ?>
    </div>
  </div>
  <br>
  <div id="djelatnikcontainer">
    <div id="seconddjel">
        <img src="src/djel4.jpg" width="330" height="300" style="border-radius:90%;box-shadow: 2px 2px 5px grey;" alt="Djelatnik2">
        <strong>Iva</strong>
     <?php 
     $sqldjel="SELECT ROUND(AVG(ocjenadjelatnika), 1) AS avg FROM comments WHERE djelatnik='Iva'";
     $result=mysqli_query($con,$sqldjel);
     while($row = mysqli_fetch_array($result)) { 
       echo"<div><br>".$row['avg']." <i class='fas fa-star' style='color:gold;text-shadow: 2px 2px 4px grey;'></i></div>"; 
      }
     ?>
    </div>
  </div>
  <br>
  <div id="djelatnikcontainer">
    <div id="thirddjel">
      <img src="src/djel3.jpg" width="330" height="300" style="border-radius:90%;box-shadow: 2px 2px 5px grey;" alt="Djelatnik3">
      <strong>Jasna</strong>
      <?php 
        $sqldjel="SELECT ROUND(AVG(ocjenadjelatnika), 1) AS avg FROM comments WHERE djelatnik='Jasna'";
        $result=mysqli_query($con,$sqldjel);
        while($row = mysqli_fetch_array($result)) { 
          echo"<div><br>".$row['avg']." <i class='fas fa-star' style='color:gold;text-shadow: 2px 2px 4px grey;'></i></div>"; 
        }
      ?>
    </div>
  </div>
</div>
<br><br>



<br>
<!--    ....ČLANCI....   -->
<?php
  $querynovosti="SELECT id, title, about, img FROM articles";
  $response = mysqli_query($con,$querynovosti);

  echo "<div id='homenovosti'>
  <h1 id='naslovnovosti'>Novosti</h1>";

  if($response){
    while($row=mysqli_fetch_array($response)){
      
        echo "
        <div id='containernovosti'>
        <p id='aboutnovosti'>
            <img id='uploaded_img' src='uploads/".$row['img']."'>
            <strong>".$row['title']."</strong><br>
            ".$row['about']."
        </p>
        </div><br>";
        if($row['id']==2){
          break;
        }
    }
  }
  echo "
  <h5 id='smallnovosti'>Više članaka možete pročitati na Novostima</h5>
  </div>";
?>
<br><br><br>







<!--     ....KOMENTARI....    -->
<div class="m-auto" style="display:table;">
  <h1 id="glavninaslov">Vaši komentari</h1>
</div><br>
<?php if(isset($_SESSION["loggedin"])===true){ 
  $x=1;
  if($_SESSION["is_admin"]!=$x){ ?>
  <div id="comment">
  <form name="myform" method="post" action="unesikomentar.php">
    <h4>Ovdje možete ostaviti svoj komentar</h4>
    <textarea id="new_comm" name="komentar" rows="3" cols="30" placeholder="Ovdje upišite svoj komentar"></textarea>
    
    <h5>Ocjenite uslugu: </h5>
    <select class="form-control" id="ocjenausluge" name="ocjenausluge">
        <option>5</option>
        <option>4</option>
        <option>3</option>
        <option>2</option>
        <option>1</option>
    </select>
    <h5>Ocjenite djelatnika: </h5>
    <select class="form-control" id="djelatnik" name="djelatnik">
      <option>Emilija</option>
      <option>Jasna</option>
      <option>Iva</option>
    </select>
    <select class="form-control" id="ocjenadjelatnika" name="ocjenadjelatnika">
        <option>5</option>
        <option>4</option>
        <option>3</option>
        <option>2</option>
        <option>1</option>
    </select>
    <button class="btn btn-outline-primary m-2" type="submit" name="submit">Submit</button>
  </form>
  </div>
<?php } } ?>
<br>
<div id='vecikomentari'>
<?php 
    $sql = "SELECT c.idC, c.komentar, c.ocjenausluge, c.djelatnik, c.ocjenadjelatnika, c.memberid, m.id, m.firstname, 
    m.lastname FROM comments AS c INNER JOIN members AS m ON(c.memberid=m.id)";
    
    $result=$con->query($sql);
    
    if($result->num_rows > 0){
        while($row = $result->fetch_assoc()){
        $idc=$row["idC"];
        $komentar=$row["komentar"];
        $ocjenausluge=$row["ocjenausluge"];
        $djelatnik=$row["djelatnik"];
        $ocjenadjelatnika=$row["ocjenadjelatnika"];
        $memberid= $row["memberid"];
        $ime = $row['firstname'];
        $prezime = $row['lastname'];
        echo"<div id='komentari'>   
        <form method='post' action='deletecomment.php' enctype='multipart/form-data'>    
        <input type='hidden' name='id' value='$idc'>
        <strong>$ime $prezime</strong> <br>
        $komentar<br>
        <strong>Ocjena usluge:</strong> $ocjenausluge <i class='fas fa-star' style='color:gold;text-shadow: 2px 2px 4px grey;'></i><br>
        <strong>Ocjena djelatnika:</strong> $djelatnik $ocjenadjelatnika <i class='fas fa-star' style='color:gold;text-shadow: 2px 2px 4px grey;'></i><br>
        ";
        if(isset($_SESSION["loggedin"])===true){ 
        $x=1;
        if($_SESSION["is_admin"]==$x){ 
        echo"
        <button name='delete' type='submit' style='border:none;background-color:white;float:right;'><i class='fas fa-trash-alt' style='font-size:20px'></i></button>
        "; 
        } }
        echo" 
        </form>
        </div>";
        }
    }
    ?> 
</div>


<!--   ....Footer....   -->
<footer class="text-center text-lg-start bg-light text-muted" id="footer_kontakt">
  <section class="">
    <div class="container text-center text-md-start">
      <div class="row mt-3">
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-2">
            <h6 class="text-uppercase fw-bold my-4 ">
                Kontakt
            </h6>
            <p><i class="fas fa-home me-3"></i> Osijek, Vukovarska 1, Hrvatska</p>
            <p>
                <i class="fas fa-envelope me-3"></i>
                info@frizerskisalonm.com
            </p>
            <p><i class="fas fa-phone me-3"></i> + 0123 567 288</p>
            <p><i class="fas fa-print me-3"></i> + 0123 567 289</p>
        </div>


        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-2">
          <h6 class="text-uppercase fw-bold my-4">
            Radno vrijeme
          </h6>
          <p>
              <strong>ponedjeljak - petak: </strong> 08:00 - 20:00 sati
          </p>
          <p>
              <strong> subota: </strong> 08:00 - 14:00 sati
          </p>
          <p>
              <strong> nedjelja i blagdani: </strong> ne radimo 
          </p>
        </div>
      </div>
     
    </div>
  </section>
</footer>
<!--  ....UP BUTTON....   -->
<button onclick="topFunction()" id="upBtn"><i class='fas fa-angle-up' style='font-size:20px'></i></button> 
    

</body>
</html>

<style>

  /**   ABOUT    */
  #glavninaslov{
    font-family: 'Dancing Script', cursive;
    margin: 4px;

  }
  #abouthairsaloon{
    margin: auto;
    justify-content: center;
  }
  #smallerdiv{
    margin: auto;
    display: flex;
  
    width: 50%;
    padding: 8px;
    box-shadow: 2px 2px 5px grey;
  }
  #naslov{
    position: relative;
  }


  /** FOOTER */
#footer_kontakt{ 
    clear: both;
    position: inherit;
    left: 0;
    bottom: 0;
    width: 100%; 
}

/**  DJELATNICI */
#bigdjelcont{
  width:100%;
  margin: auto;
  justify-content: center;
  background-image: url("src/yellow.jpg");
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
}
#djelatnikcontainer{
  display: flex;
  width:50%;
  margin:auto;
  font-family: 'Dancing Script', cursive;
  font-size: 40px;
}
#firstdjel{
  
  clear: both;
  display: flex;
  width: 50%;
  margin-left: auto;
  /**content: "";*/
  justify-content: center;
  flex-wrap: wrap;
  align-content:center;
  text-align: center;
}
#seconddjel{
  clear: both;
  display: flex;
  margin-right: auto;
  width: 50%;
  flex-wrap: wrap;
  justify-content: center;
  align-content:center;
}
#thirddjel{
  clear: both;
  display: flex;
  float:none;
  width: 50%;
  margin-left: auto;
  justify-content: center;
  flex-wrap: wrap;
  align-content:center;
}



/** KOMENTARI */
#comment{
  width: 40%;
  margin: 10px;
  padding: 4px;
  box-shadow: 2px 2px 5px grey;
  font-family: Arial, Helvetica, sans-serif;
}

#new_comm{
    width: 95%;
    margin: 5px;
    padding: 15px;
    border: none;
    background: #f1f1f1;
    margin: 4px;
}
#new_comm:focus{
    background-color: #ddd;
    outline: none;
}
#djelatnik, #ocjenadjelatnika, #ocjenausluge{
    width: 40%;
    margin: 10px 10px;
    border: none;
    background: #f1f1f1;
    margin: 4px;
}
#djelatnik:focus, #ocjenadjelatnika:focus, #ocjenausluge:focus{
  background-color: #ddd;
  outline: none;
}
#vecikomentari{
  display: flex;
  margin: auto;
}
#komentari{
  overflow: hidden;
  width: 18%;
  margin: 14px;
  padding: 4px;
  box-shadow: 2px 2px 5px grey;
  font-family: Arial, Helvetica, sans-serif;
  flex-wrap: wrap;
}


/*  NOVOSTI  */ 
#homenovosti{
  margin: auto;
  text-align: center;
}
#naslovnovosti{
  font-family: 'Dancing Script', cursive;
  margin: 8px;
}
#smallnovosti{
  margin:8px;
  color: #696969;
  font-family: 'Dancing Script', cursive;
}

#containernovosti{
    display:flex;
    width: 70%;
    margin: auto;
    box-shadow: 2px 2px 5px grey;
    padding: 5px;
    flex-wrap: wrap;
    font-family: Arial, Helvetica, sans-serif;
}
#aboutnovosti{
    padding: 5px;
    margin: auto;
    text-align: justify;
}
#uploaded_img{
    height: 30%;
    width: 25%;
    padding: 5px 15px 5px 5px;
    margin: auto;
    float: left;
}

/** CAROUSEL */
.carousel-inner img {
  width: 100%;
  height: 850px;
}

/**UPP BUTTON */
#upBtn {
  width:45px;
  display: none;
  position: fixed;
  bottom: 20px;
  right: 30px;
  z-index: 99;
  font-size: 10px;
  border: none;
  outline: none;
  background-color: #FF7F50;
  color: white !important;
  cursor: pointer;
  padding: 14px;
  border-radius: 50%;
}
</style>


<script>
//Get the button
    var mybutton = document.getElementById("upBtn");

    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "block";
    } else {
        mybutton.style.display = "none";
    }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    window.pageYOffset=0;
    }
    
    
</script>