<?php
include("db.php");
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reset Password</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4"></div>
                <div class="col-md-4">
                <?php
                include('db.php');
                if(isset($_GET["key"]) && isset($_GET["email"]) && isset($_GET["action"]) && ($_GET["action"] == "reset") && !isset($_POST["action"])) {
                    $key = $_GET["key"];
                    $email = $_GET["email"];
                    $curDate = date("Y-m-d H:i:s");

                    $queryy="SELECT * FROM password_reset_temp WHERE keyy='$key' AND email='$email'";
                    $result = mysqli_query($con, $queryy);
                    $row = mysqli_num_rows($result);  
      
                    if($row=="") {
                        echo'Invalid Link';
                    }else{
                        $row = mysqli_fetch_assoc($result);
                        $expDate = $row['expDate'];
                      
                        if ($expDate >= $curDate) {
                            ?> 
                            <h2>Reset Password</h2>   
                            <form method="post" action="" name="update">
                                <input type="hidden" name="action" value="update" class="form-control"/>
                                <div class="form-group">
                                    <label><strong>Enter New Password:</strong></label>
                                    <input type="password"  name="pass1" value="" class="form-control"/>
                                </div>
                                <div class="form-group">
                                    <label><strong>Re-Enter New Password:</strong></label>
                                    <input type="password"  name="pass2" value="" class="form-control"/>
                                </div>
                                <input type="hidden" name="email" value="<?php echo $email; ?>"/>
                                <div class="form-group">
                                    <input type="submit" id="reset" value="Reset Password"  class="btn btn-primary"/>
                                </div>

                            </form>
                            <?php
                            } else {
                                $error .= "<h2>Link Expired</h2>";
                            }
                        }
                
                }


                    if (isset($_POST["email"]) && isset($_POST["action"]) && ($_POST["action"] == "update")) {
                        $error = "";
                        $pass1 = mysqli_real_escape_string($con, $_POST["pass1"]);
                        $pass2 = mysqli_real_escape_string($con, $_POST["pass2"]);
                        $email = $_POST["email"];
                        
                        if ($pass1 != $pass2) {
                            $error .= "<p>Password do not match, both password should be same.<br /><br /></p>";
                        }
                        if ($error != "") {
                            echo $error;
                        } else {

                            $hashed_pass = password_hash($pass1, PASSWORD_DEFAULT);
    
                            mysqli_query($con, "UPDATE members SET pass = '".$hashed_pass."' WHERE email = '".$email."'");

                            mysqli_query($con, "DELETE FROM 'password_reset_temp' WHERE email = '$email'");

                            echo "<script>alert('Congratulations! Your password has been updated successfully.')</script>";
                            mysqli_close($con);
                            echo "<script>window.open('login.php', '_self')</script>";
                        }
                    }
                    ?>

                </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</body>
</html>